# -*- coding: utf-8 -*-
from distutils.core import setup
import io
import re
import os


DOC = '''TODO'''


def read(*names, **kwargs):
    return io.open(
        os.path.join(os.path.dirname(__file__), *names),
        encoding=kwargs.get("encoding", "utf8")
    ).read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(name='bgit',
      version=find_version('bgit/__init__.py'),
      description=u'git repositories batch pull tool',
      long_description=DOC,
      author='oschina',
      author_email='oschina.net@gmail.com',
      url='https://gitee.com/oschina/bgit',
      license='MIT',
      install_requires=[
      ],
      classifiers=[
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Natural Language :: Chinese (Simplified)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Utilities'
      ],
      keywords='bgit, git, gitee, github',
      zip_safe=False,
      packages=['bgit'],
      package_data={'': ['config.ini']},
      include_package_data=True,
)