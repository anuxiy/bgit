# bgit

bgit 是一个批量拉取 git 仓库的工具。当前支持 gitee 和 github 两个平台。

使用方法：

```shell
bgit -u xxx -p xxxx [options] https://gitee.com/xxxx
bgit -u xxx -p xxxx [options] https://github.com/xxxx
```

该命令将 gitee 和 github 上指定地址下的所有仓库拉取到当前路径，每个仓库一个独立的目录存放。

可以使用 `bgit --help` 查看更多参数信息

```shell
-u 登录账号(邮箱或其他有效标识)
-p 登录密码
-o 只拉取个人仓库，不包括 fork 的仓库
-z 只拉取私有仓库
-y 只拉取公开仓库
```

要求：

- Python 3.x